FROM caddy:builder AS builder

RUN xcaddy build \
    --with github.com/greenpau/caddy-security \
    --with github.com/caddy-dns/njalla \
    --with github.com/caddy-dns/glesys

FROM caddy

COPY --from=builder /usr/bin/caddy /usr/bin/caddy
